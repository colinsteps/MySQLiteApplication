package com.example.dev.mysqliteapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ContactDetailsActivity extends AppCompatActivity {

    private EditText editText_name;
    private EditText editText_email;
    private EditText editText_phone;
    private Button button_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);

        editText_name = (EditText) findViewById(R.id.editText_name);
        editText_email = (EditText) findViewById(R.id.editText_email);
        editText_phone = (EditText) findViewById(R.id.editText_phone);
        button_save = (Button) findViewById(R.id.button_save);

        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveContact();
            }
        });
    }

    private void saveContact() {
        DBHelper db = new DBHelper(this);
        Contact newContact = new Contact();
        newContact.setName(editText_name.getText().toString());
        newContact.setEmail(editText_email.getText().toString());
        newContact.setPhone(editText_phone.getText().toString());

        db.insertContact(newContact);
        finish();
    }
}
